# nginx_loadbalancer role

This installes nginx with the sole purpose of acting as a TCP loadbalancer.

## Variables

* `nginx_loadbalancer_port`

    The port the loadbalancer listens on for incoming requests.


* `nginx_loadbalancer_servers`

    A list of servers to forward requests to. IP and port of each server.

    Example: [ '192.168.1.1:8000', '192.168.1.2:8000', '192.168.1.3:8000' ]
